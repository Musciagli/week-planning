package it.uninsubria.week_planning;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import it.uninsubria.week_planning.model.Food;

public class DBhelper extends SQLiteOpenHelper {

    public static final String DBNAME="WeekPlanningDB";
    private static final int DB_VERSION = 1;
    private static final String TABLE_FOODS = "foods";
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_CALORIES = "calories";
    private static final String KEY_FOODIMAGE = "foodimage";
    private static final String KEY_DATE = "date";
    private static final String KEY_PUBLISHED = "published";
    private static DBhelper sInstance;

    public static synchronized DBhelper getInstance(Context context)
    {
        if(sInstance == null)
            sInstance = new DBhelper(context.getApplicationContext());
        return sInstance;
    }

    private DBhelper(Context context) {
        super(context, DBNAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CrateFoodTableQuery = "CREATE TABLE foods( " +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "name TEXT, "+
                "calories INTEGER, "+
                "foodimage TEXT, "+
                "date TEXT, "+
                "published TEXT)";

        sqLiteDatabase.execSQL(CrateFoodTableQuery);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS foods");
        this.onCreate(sqLiteDatabase);
    }

    public Food getFoodByName(String name){
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "select * from " + TABLE_FOODS + " where name = ?";
        Cursor cursor = db.rawQuery(query, new String[] {name});

        if (cursor != null)
            cursor.moveToFirst();

        Integer retrieved_id = Integer.parseInt(cursor.getString(0));
        String retrieved_name = cursor.getString(1);
        Integer retrieved_calories = Integer.parseInt(cursor.getString(2));
        String retrieved_foodimage = cursor.getString(3);
        String retrieved_date = cursor.getString(4);
        String retrieved_published_temp = cursor.getString(5);
        Boolean retrieved_published;
        if(retrieved_published_temp=="true")
            retrieved_published = true;
        else
            retrieved_published = false;
        Food food = new Food(retrieved_id, retrieved_name, retrieved_calories, retrieved_foodimage, retrieved_date, retrieved_published);

        Log.d("getFood("+name+")", food.toString());
        return food;
    }

    public List<Food> getAllFoodsByDate(String date){
        SQLiteDatabase db = this.getReadableDatabase();
        List<Food> foods = new LinkedList<Food>();

        String query = "select * from " + TABLE_FOODS + " where date = ?";
        Cursor cursor = db.rawQuery(query, new String[] {date});

        if (cursor.moveToFirst()) {
            do {
                Integer retrieved_id = Integer.parseInt(cursor.getString(0));
                String retrieved_name = cursor.getString(1);
                Integer retrieved_calories = Integer.parseInt(cursor.getString(2));
                String retrieved_foodimage = cursor.getString(3);
                String retrieved_date = cursor.getString(4);
                String retrieved_published_temp = cursor.getString(5);
                Boolean retrieved_published;
                if(retrieved_published_temp.equals("true"))
                    retrieved_published = true;
                else
                    retrieved_published = false;
                Food food = new Food(retrieved_id, retrieved_name, retrieved_calories, retrieved_foodimage, retrieved_date, retrieved_published);

                foods.add(food);
            } while (cursor.moveToNext());
        }
        return foods;
    }

    public void addFood(Food food){
        Log.d("addFood", food.toString());

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("INSERT INTO foods (name, calories, foodimage, date, published) VALUES (?, ?, ?, ?, ?)", new String[] {food.getName(), food.getCalories().toString(), food.getFoodImage(), food.getDate(), ""+food.getPublished()});

        db.close();
    }

    public void updatePublishedFood(String date) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues newValues = new ContentValues();
        newValues.put(KEY_PUBLISHED, "true");
        db.update(TABLE_FOODS, newValues, "date = ?", new String[]{date});

        db.close();
    }

    public void deleteFood(Food food) {
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_FOODS,
                KEY_ID+" = "+food.getId(),
                new String[] { String.valueOf(food.getId()) });

        db.close();
    }

    public void deleteAllFoods() {
        SQLiteDatabase db = this.getWritableDatabase();

        db.execSQL("delete from "+ TABLE_FOODS);

        db.close();
    }
}
