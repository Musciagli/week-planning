package it.uninsubria.week_planning.model;

public class Food {
    private Integer id;
    private String name;
    private Integer calories;
    private String foodImage;
    private String date;
    private boolean published;

    public Food(Integer id, String name, Integer calories, String foodImage, String date, boolean published)
    {
        this.id = id;
        this.name = name;
        this.calories = calories;
        this.foodImage = foodImage;
        this.date = date;
        this.published = published;
    }

    public Integer getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public Integer getCalories() {
        return calories;
    }
    public String getFoodImage() {
        return foodImage;
    }
    public String getDate() {
        return date;
    }
    public boolean getPublished(){ return published; }
    public void setDate(String date)
    {
        this.date = date;
    }
    public void setPublished() { this.published = true; }
}