package it.uninsubria.week_planning.ui.dayView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import it.uninsubria.week_planning.DBhelper;
import it.uninsubria.week_planning.R;
import it.uninsubria.week_planning.model.Food;

public class DayFragment extends Fragment implements AddFoodCallback, PushFoodCallback {

    TextView dayTextView;
    RecyclerView foodListView;
    FoodListAdapter adapter;
    LinearLayoutManager layoutManager;
    FloatingActionButton addButton;
    FloatingActionButton pushButton;
    ImageView isOnCalendar;
    Calendar day;

    List<Food> foodList = new LinkedList<>();

    public DayFragment (Calendar day)
    {
        this.day = day;
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_day, container, false);
        addButton = root.findViewById((R.id.add_button));
        pushButton = root.findViewById((R.id.push_button));
        dayTextView = root.findViewById(R.id.text_day);
        foodListView = root.findViewById(R.id.foodList);
        isOnCalendar = root.findViewById(R.id.isOnCalendar);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddFoodDialog();
            }
        });

        pushButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPushFoodDialog();
            }
        });

        initRecycleView();
        writeDay();
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onResume(){
        super.onResume();
        listRefresh();
    }

    private void listRefresh() {
        foodList = DBhelper.getInstance(getContext()).getAllFoodsByDate(""+day);
        adapter.setFoodList(foodList);
    }

    private void writeDay()
    {
        SimpleDateFormat dateform = new SimpleDateFormat("dd/MM/yyyy");
        Date datetemp = day.getTime();
        String dateString = dateform.format(datetemp);
        dayTextView.setText("Food list of " + dateString);
    }

    private void initRecycleView()
    {
        layoutManager = new LinearLayoutManager(getContext());
        foodListView.setLayoutManager(layoutManager);

        adapter = new FoodListAdapter();
        foodListView.setAdapter(adapter);
    }

    private void showAddFoodDialog() {
        FragmentManager fm = getFragmentManager();
        AddFoodDialogFragment addFoodDialog = new AddFoodDialogFragment(this);
        addFoodDialog.setCancelable(false);
        addFoodDialog.show(fm, "fragment_addFood");
    }

    private void showPushFoodDialog() {
        FragmentManager fm = getFragmentManager();
        PushFoodDialogFragment pushFoodDialog = new PushFoodDialogFragment(this);
        pushFoodDialog.setCancelable(false);
        pushFoodDialog.show(fm, "fragment_pushFood");
    }


    public void onFoodAdded(Food food) {
        addFoodList(food);
        addFoodDB(food, day);
    }

    public void onFoodPushed(String eventName)
    {
        addFoodCalendar(eventName);
    }

    private void addFoodList(Food food) {

        foodList.add(food);
        adapter.setFoodList(foodList);
    }

    private void addFoodDB(Food food, Calendar day) {

        food.setDate(""+day);
        DBhelper.getInstance(getContext()).addFood(food);
    }

    private void addFoodCalendar(String eventName) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(day.getTime());

        String foodListDesc = "";
        int i = 0;
        for(Food f : foodList) {
            if (!f.getPublished())
            {
                f.setPublished();
                if(i++ == foodList.size() - 1)
                    foodListDesc = foodListDesc +  f.getName();
                else
                    foodListDesc = foodListDesc +  f.getName() + " - ";
            }
        }

        Calendar beginTime = Calendar.getInstance();
        beginTime.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE));
        Calendar endTime = Calendar.getInstance();
        endTime.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE));

        Intent intent = new Intent(Intent.ACTION_INSERT)
                .setData(CalendarContract.Events.CONTENT_URI)
                .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginTime.getTimeInMillis())
                .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime.getTimeInMillis())
                .putExtra(CalendarContract.Events.TITLE, eventName)
                .putExtra(CalendarContract.Events.DESCRIPTION, foodListDesc);
        startActivityForResult(intent, 50);

        DBhelper.getInstance(getContext()).updatePublishedFood(""+day);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            listRefresh();
        }
    }
}