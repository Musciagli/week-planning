package it.uninsubria.week_planning.ui.dayView;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import it.uninsubria.week_planning.R;
import it.uninsubria.week_planning.model.Food;

public class FoodListAdapter extends RecyclerView.Adapter<FoodListAdapter.FoodViewHolder> {

    private List<Food> foodList = new ArrayList<>();

    public class FoodViewHolder extends RecyclerView.ViewHolder {
        TextView foodNameTextView;
        TextView caloriesTextView;
        ImageView foodImage;
        ImageView isOnCalendar;

        public FoodViewHolder(@NonNull View itemView) {
            super(itemView);
            this.foodNameTextView = itemView.findViewById(R.id.foodNameTextView);
            this.caloriesTextView = itemView.findViewById(R.id.caloriesTextView);
            this.foodImage = itemView.findViewById(R.id.food_image);
            this.isOnCalendar = itemView.findViewById(R.id.isOnCalendar);
        }
    }

    public void setFoodList(List<Food> foodList) {
        this.foodList = foodList;
        this.notifyDataSetChanged();
    }

    //Crea la View per la cella specificata
    @NonNull
    @Override
    public FoodViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = (View) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.food_item, parent, false);
        FoodViewHolder vh = new FoodViewHolder(v);
        return vh;
    }

    //Viene chiamato per ogni cella da visualizzare
    @Override
    public void onBindViewHolder(@NonNull FoodViewHolder holder, int position) {
        Food tempFood = foodList.get(position);

        if (!tempFood.getFoodImage().equals("")) {
            Bitmap photo = BitmapFactory.decodeFile(tempFood.getFoodImage());
            holder.foodImage.setImageBitmap(photo);
        }

        holder.foodNameTextView.setText(tempFood.getName());
        holder.caloriesTextView.setText(tempFood.getCalories().toString()+" cal");
        if(tempFood.getPublished() == false)
            holder.isOnCalendar.setImageResource(R.drawable.ic_event_busy_black_18dp);
        else
            holder.isOnCalendar.setImageResource(R.drawable.ic_event_available_appcolor_18dp);
    }

    //Viene chiamato ad ogni aggiornamento della lista, ritorna il numero di celle da visualizzare
    @Override
    public int getItemCount() {
        return foodList.size();
    }
}