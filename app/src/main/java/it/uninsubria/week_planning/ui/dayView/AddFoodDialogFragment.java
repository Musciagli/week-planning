package it.uninsubria.week_planning.ui.dayView;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import android.os.Environment;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import it.uninsubria.week_planning.R;
import it.uninsubria.week_planning.model.Food;

interface AddFoodCallback {
    void onFoodAdded(Food food);
}

public class AddFoodDialogFragment extends DialogFragment {

    private AddFoodCallback callback;
    EditText nameText;
    EditText caloriesText;
    ImageView cameraButton;

    private String imageFileLocation = "";

    public AddFoodDialogFragment(AddFoodCallback callback) {
        this.callback = callback;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_add_food_dialog, container, false);
        Button addButton = view.findViewById(R.id.button_add_food);
        Button cancelButton = view.findViewById(R.id.button_cancel);
        cameraButton = view.findViewById(R.id.camera_button);
        nameText = view.findViewById(R.id.add_name_text);
        caloriesText = view.findViewById(R.id.add_calories_text);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeDialog();
                return;
            }
        });

        cameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ContextCompat.checkSelfPermission(
                        getContext(), Manifest.permission.CAMERA) ==
                        PackageManager.PERMISSION_GRANTED) {

                    Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
                    File photoFile = null;
                    try {
                        photoFile = createImageFile();
                    }catch (IOException e) {
                        e.printStackTrace();
                    }
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                    startActivityForResult(intent, 200);
                }
                else {
                    requestPermissions(new String[] { Manifest.permission.CAMERA },
                            100);
                }
            }
        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String caloriesTextNew = caloriesText.getText().toString();
                String nameText2 = nameText.getText().toString();
                if (nameText2.isEmpty()) {
                    Toast errorToast = Toast.makeText(getContext(),"Please write food name", Toast.LENGTH_LONG);
                    errorToast.show();
                    return;
                }
                try {
                    Integer caloriesNumber = Integer.valueOf(caloriesTextNew);
                    callback.onFoodAdded(new Food(0, nameText2, caloriesNumber, imageFileLocation, "", false));
                    closeDialog();
                } catch (Exception e) {
                    Toast errorToast = Toast.makeText(getContext(),"Please write calories", Toast.LENGTH_LONG);
                    errorToast.show();
                }
            }
        });
        return view;
    }

    private void closeDialog() {
        dismiss();
    }

    @Override
    public void onResume() {
        super.onResume();
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            Bitmap photo = BitmapFactory.decodeFile(imageFileLocation);
            cameraButton.setImageBitmap(photo);
        }
    }

        File createImageFile() throws IOException {
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = "IMAGE_" + timeStamp + "_";
            File storageDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

            File image = File.createTempFile(imageFileName, ".jpg", storageDirectory);
            imageFileLocation = image.getAbsolutePath();

            return image;
        }
}