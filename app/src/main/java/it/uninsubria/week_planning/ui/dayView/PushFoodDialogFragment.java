package it.uninsubria.week_planning.ui.dayView;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;

import it.uninsubria.week_planning.R;

interface PushFoodCallback {
    void onFoodPushed(String nametext);
}

public class PushFoodDialogFragment extends DialogFragment {

    private PushFoodCallback callback;
    EditText eventNameText;

    public PushFoodDialogFragment(PushFoodCallback callback) {
        this.callback = callback;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_push_food_dialog, container, false);
        Button addButton = view.findViewById(R.id.button_add_event);
        Button cancelButton = view.findViewById(R.id.button_cancel);
        eventNameText = view.findViewById(R.id.add_event_text);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                closeDialog();
                return;
            }
        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    String nameText2 = eventNameText.getText().toString();
                    if (nameText2.isEmpty()) {
                        Toast errorToast = Toast.makeText(getContext(),"Please write event name", Toast.LENGTH_LONG);
                        errorToast.show();
                        return;
                    }
                    callback.onFoodPushed(nameText2);
                    closeDialog();
                }
        });
        return view;
    }

    private void closeDialog() {
        dismiss();
    }

    @Override
    public void onResume() {
        super.onResume();
        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
        params.width = ViewGroup.LayoutParams.MATCH_PARENT;
        params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);
    }
}